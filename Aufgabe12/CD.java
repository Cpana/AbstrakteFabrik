package De.thk.eks.p1.Aufgabe12;

public class CD extends Artikel{
    public CD(String name, double preis, int produktNr) {
        super(name, preis, produktNr);
    }

    @Override
    public Artikel erzeugeArtikel(Artikel a) {
        return new CD(a.getName(),a.getPreis(), a.getProduktNr());
    }
}
