package De.thk.eks.p1.Aufgabe12;


abstract class Artikel{

    private String name;
    private double preis;
    private int produktNr;

    public Artikel(String name, double preis, int produktNr) {
        this.name = name; this.preis = preis; this.produktNr = produktNr; }

    public abstract Artikel erzeugeArtikel(Artikel a);

    public String getName() { return name; }
    public double getPreis() { return preis; }
    public int getProduktNr() { return produktNr; }

}
