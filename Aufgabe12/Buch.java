package De.thk.eks.p1.Aufgabe12;

public class Buch extends Artikel{

    public Buch(String name, double preis, int produktNr) {
        super(name, preis, produktNr);
    }

    @Override
    public Artikel erzeugeArtikel(Artikel a) {
        return new Buch(a.getName(),a.getPreis(),a.getProduktNr());
    }

}
