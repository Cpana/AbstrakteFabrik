package De.thk.eks.p1.Aufgabe12;

import java.util.ArrayList;
import java.util.List;

public abstract class Subjekt {

    List<Beobachter> beobachterList = new ArrayList<>();

    public void meldeAn(Beobachter b) {
        this.beobachterList.add(b);
        b.setSubjekt(this);
    }


    public void meldeAb(Beobachter b) {
        this.beobachterList.remove(b);
        b.setSubjekt(null);
    }

    public void benachrichtige() {
        for(Beobachter beobachter:beobachterList){
            beobachter.aktualisiere();
        }
    }
}
