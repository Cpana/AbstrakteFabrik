package De.thk.eks.p1.Aufgabe12;

public class DVD extends Artikel{
    public DVD(String name, double preis, int produktNr) {
        super(name, preis, produktNr);
    }

    @Override
    public Artikel erzeugeArtikel(Artikel a) {
        return new DVD(a.getName(),a.getPreis(), a.getProduktNr());
    }
}
