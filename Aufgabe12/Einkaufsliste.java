package De.thk.eks.p1.Aufgabe12;

import java.util.ArrayList;

public class Einkaufsliste extends Subjekt {

    private ArrayList<Artikel> eArtikel;

    public Einkaufsliste(){
        eArtikel = new ArrayList<>();
    }

    public void addArtikel(Artikel artikel){
        eArtikel.add(artikel);
        benachrichtige();
    }

    public void delArtikel(Artikel artikel){
        eArtikel.remove(artikel);
        benachrichtige();
    }

    public ArrayList<Artikel> gibZustand(){
        return eArtikel;
    }






}
