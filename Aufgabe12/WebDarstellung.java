package De.thk.eks.p1.Aufgabe12;

import java.util.ArrayList;

public class WebDarstellung extends Beobachter {

    private ArrayList<Artikel> alteEinkaufsliste = new ArrayList<>();
    private ArrayList<Artikel> neueEinkaufsListe;


    public WebDarstellung(Einkaufsliste eListe) {
        neueEinkaufsListe = new ArrayList<>();

    }

    @Override
    public void aktualisiere() {
        neueEinkaufsListe = ((Einkaufsliste) subjekt).gibZustand();
        zeigeAn();
    }

    public void zeigeAn() {
        if(neueEinkaufsListe.size() > alteEinkaufsliste.size()){
            System.out.println("Hinzugefuegter Artikel " + neueEinkaufsListe.get(neueEinkaufsListe.size()-1).getName() + " Preis: " + neueEinkaufsListe.get(neueEinkaufsListe.size()-1).getPreis());
            alteEinkaufsliste.add(neueEinkaufsListe.get(neueEinkaufsListe.size()-1));
        }else if (neueEinkaufsListe.size() < alteEinkaufsliste.size()){
            System.out.println("Entfernter Artiekl " + alteEinkaufsliste.get(neueEinkaufsListe.size()-1).getName() + " Preis: " + alteEinkaufsliste.get(neueEinkaufsListe.size()-1).getPreis());
            alteEinkaufsliste.remove(neueEinkaufsListe.size()-1);
        }
    }
}
