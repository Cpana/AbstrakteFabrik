package De.thk.eks.p1.Aufgabe12;

import java.util.ArrayList;

public class main {
    public static void main(String args []){

        Einkaufsliste eliste = new Einkaufsliste();
        Rechnung rechnung = new Rechnung();
        WebDarstellung webDarstellung = new WebDarstellung(eliste);
        eliste.meldeAn(webDarstellung);
        eliste.meldeAn(rechnung);

        CD cd3 = new CD("CD3", 25, 12);
        CD cd4 = new CD("CD4", 8, 1);
        DVD dvd = new DVD("DVD2", 145, 2);
        Buch buch = new Buch("Buch4", 16, 3);
        Buch buch2 = new Buch("Buch5", 76, 5);



        eliste.addArtikel(cd3);
        eliste.addArtikel(cd4);
        eliste.addArtikel(dvd);
        eliste.delArtikel(cd4);
        eliste.delArtikel(cd3);
        eliste.addArtikel(buch);
        eliste.addArtikel(cd4);
        eliste.addArtikel(buch2);







    }
}
