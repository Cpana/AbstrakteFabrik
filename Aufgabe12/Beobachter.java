package De.thk.eks.p1.Aufgabe12;

public abstract class Beobachter {

    Subjekt subjekt;

    public void setSubjekt(Subjekt subjekt) {
        this.subjekt = subjekt;
    }

    public abstract void aktualisiere();

}
