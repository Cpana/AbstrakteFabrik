package De.thk.eks.p1.Aufgabe12;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Rechnung extends Beobachter {

        ArrayList<Artikel> einkaufsListeZustand = new ArrayList<>();

        private void zustandEListe(ArrayList<Artikel> a){
        double gesamtpreis = 0;
        for (int i = 0; i < a.size(); i++){
            gesamtpreis += a.get(i).getPreis();
        }
        System.out.println("Gesamtpreis der Einkaufliste:" +gesamtpreis);
    }

    @Override
    public void aktualisiere() {
        einkaufsListeZustand = ((Einkaufsliste) subjekt).gibZustand();
        zustandEListe(einkaufsListeZustand);
    }

}
