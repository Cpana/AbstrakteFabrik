package De.thk.eks.p1.Aufgabe11a;

public abstract class Gutschein{
    private int wert;

    public Gutschein(int wert) {
        this.wert = wert;
    }

    public int getWert() {
        return wert;
    }

}