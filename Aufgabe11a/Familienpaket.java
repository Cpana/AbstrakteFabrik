package De.thk.eks.p1.Aufgabe11a;



public class Familienpaket extends Geschenkpaket {
    @Override
    public Werbegeschenk erzeugeWerbegeschenk() {
        return new Fussball();
    }

    @Override
    public Gutschein erzeugeGutschein() {
        return new DvdGutschein(25);
    }
}
