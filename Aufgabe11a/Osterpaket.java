package De.thk.eks.p1.Aufgabe11a;



public class Osterpaket extends Geschenkpaket {
    @Override
    public Werbegeschenk erzeugeWerbegeschenk() {
        return new Osterhase();
    }

    @Override
    public Gutschein erzeugeGutschein() {
        return new CdGutschein(15);
    }
}
