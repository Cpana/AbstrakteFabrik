package De.thk.eks.p1.Aufgabe11a;

public abstract class Werbegeschenk{
    private String art;

    public Werbegeschenk(String art) {
        this.art = art;
    }

    public String getArt() {
        return art;
    }

}