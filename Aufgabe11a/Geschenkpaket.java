package De.thk.eks.p1.Aufgabe11a;

public abstract class Geschenkpaket{
    public abstract Werbegeschenk erzeugeWerbegeschenk();
    public abstract Gutschein erzeugeGutschein();
}