package De.thk.eks.p1.Aufgabe11a;



public class Weihnachtspaket extends Geschenkpaket {
    @Override
    public Werbegeschenk erzeugeWerbegeschenk() {
        return new Weihnachtsmann();
    }

    @Override
    public Gutschein erzeugeGutschein() {
        return new CdGutschein(20);
    }
}
